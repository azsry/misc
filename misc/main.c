#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <direct.h>

enum game_type {
	TYPE_NEW = 0,
	TYPE_RESUME,
	TYPE_MAX
};

enum player_type {
	PLAYER_HUMAN = 0,
	PLAYER_AI = 1,
	PLAYER_MAX
};

enum exit_codes {
	EXIT_NORMAL = 0,
	EXIT_INCORRECT_ARGS,
	EXIT_INVALID_PLAYER,
	EXIT_INVALID_DECK,
	EXIT_INVALID_SAVE,
	EXIT_INSUFFICIENT_CARDS,
	EXIT_BOARD_FULL,
	EXIT_EOI_HUMAN,
	EXIT_GENERIC_FAIL,
	EXIT_MAX
};

typedef struct {
	// h or a
	enum player_type type;
	// player num
	int8_t id;
	// current cards in hand
	char** cards;
	// number of cards in hand
	int8_t cards_num;
} player_t;

typedef struct {
	// number of rows in game board: [3-100]
	uint8_t row_count;
	// number of columns in game board: [3-100]
	uint8_t col_count;
	// array of char* to denote a 2D array
	char*** layout;
} board_t;

typedef struct {
	// board info
	board_t board;
	// pointer to player list
	player_t* players;
	// denotes which player's turn it is
	int8_t current_player;
	// denotes game type
	enum game_type type;
	// total cards in deck
	uint8_t card_count;
	// deckfile from cli
	char* deckname;
	// an array that holds the deck of cards
	char** deck;
	// index of the first unused card
	int card_index;
	// current turn - on first turn, cards can be placed anywhere. on subsequent, they must be placed next to existing cards
	int turn;

} game_info_t;

game_info_t game_info;

// Creates the initial board
void generate_board() {
	for (int i = 0; i < game_info.board.row_count; i++) {
		game_info.board.layout[i] = malloc(sizeof(char*) * game_info.board.row_count);

		for (int j = 0; j < game_info.board.col_count; j++) {
			game_info.board.layout[i][j] = malloc(sizeof(char*));
			game_info.board.layout[i][j] = "..";
		}
	}
}

// Displays current board layout
void display_board() {
	for (int i = 0; i < game_info.board.row_count; i++) {
		for (int j = 0; j < game_info.board.col_count; j++) {
			fprintf(stdout, "%s", game_info.board.layout[i][j]);
		}
		fprintf(stdout, "\n");
	}
}

// Parses a provided deck file and returns the number of cards in the deck to card_num
int parse_deckfile(char* deckname, int* card_num) {
	FILE* deck = fopen(deckname, "r");
	int _card_num = 0;

	if (deck == NULL)
		return EXIT_GENERIC_FAIL;

	char* line = malloc(sizeof(char) * 10);

	fgets(line, 10, deck);

	if (fgets != NULL)
		game_info.card_count = atoi(line);

	while (fgets(line, 10, deck)) {
		// check if value is a proper card
		line[2] = '\0';
		game_info.deck[_card_num] = malloc(sizeof(char*));
		strcpy(game_info.deck[_card_num], line);
		_card_num++;
	}

	*card_num = _card_num;

	return EXIT_NORMAL;
}

int parse_savefile(char* savefile) {
	return EXIT_NORMAL;
}

// non-0 = full, 0 = still has empty space
int is_board_full() {
	for (int i = 0; i < game_info.board.row_count; i++) {
		for (int j = 0; j < game_info.board.col_count; j++) {
			if (!strcmp(game_info.board.layout[i][j], ".."))
				return 0;
		}
	}

	return 1;
}

int save_game(char* save_name) {
	FILE* save = fopen(save_name, "w");

	if (save == NULL) {
		fprintf(stdout, "Unable to save\n");
		return 0;
	}

	fprintf(stdout, "Opened save file at %s", save_name);

	char buffer[255];
	sprintf(buffer, "%i %i %i %i\n%s", game_info.board.col_count, game_info.board.row_count, game_info.card_index, game_info.current_player, game_info.deckname);
	fputs(buffer, save);
	for (int i = 0; i < game_info.players[0].cards_num; i++) {
		fputs(game_info.players[0].cards[i], save);
	}

	fputc('\n', save);

	for (int i = 0; i < game_info.players[1].cards_num; i++) {
		fputs(game_info.players[1].cards[i], save);
	}

	fputc('\n', save);

	for (int i = 0; i < game_info.board.row_count; i++) {
		for (int j = 0; j < game_info.board.col_count; j++) {
			fputs(game_info.board.layout[i][j], save);
		}
	}
	fprintf(stdout, "Save success!\n");
	return 2;
}

enum direction {
	DIRECTION_LEFT,
	DIRECTION_RIGHT,
	DIRECTION_UP,
	DIRECTION_DOWN,
	DIRECTION_MAX
};

char* get_adjacent_cell(enum DIRECTION dir, int in_row, int in_col, int* out_row, int* out_col) {
	switch (dir) {
	case DIRECTION_LEFT:
		*out_row = mod(in_row - 1, game_info.board.row_count);
		*out_col = mod(in_col, game_info.board.col_count);
		break;
	case DIRECTION_RIGHT:
		*out_row = mod(in_row + 1, game_info.board.row_count);
		*out_col = mod(in_col, game_info.board.col_count);
		break;
	case DIRECTION_UP:
		*out_row = mod(in_row, game_info.board.row_count);
		*out_col = mod(in_col - 1, game_info.board.col_count);
		break;
	case DIRECTION_DOWN:
		*out_row = mod(in_row, game_info.board.row_count);
		*out_col = mod(in_col + 1, game_info.board.col_count);
		break;
	}

	return game_info.board.layout[*out_row][*out_col];
}

int mod(int a, int b)
{
	int r = a % b;
	return r < 0 ? r + b : r;
}

// non-zero = valid
// zero = not valid
int check_move_validity(int8_t row, int8_t col) {
	if (row > game_info.board.row_count || col > game_info.board.col_count)
		return 0;

	if (strcmp(game_info.board.layout[row][col], ".."))
		return 0;

	int adj_col = 0, adj_row = 0;

	// at least one adjacent card must be occupied
	if (strcmp(get_adjacent_cell(DIRECTION_UP, row, col, &adj_row, &adj_col), "..") ||
		strcmp(get_adjacent_cell(DIRECTION_LEFT, row, col, &adj_row, &adj_col), "..") ||
		strcmp(get_adjacent_cell(DIRECTION_RIGHT, row, col, &adj_row, &adj_col), "..") ||
		strcmp(get_adjacent_cell(DIRECTION_DOWN, row, col, &adj_row, &adj_col), ".."))
		return 1;

	return 0;
}

int parse_move(char* in, char*** out) {
	if (strstr(in, "SAVE")) {
		// save game
		char input[255];
		strcpy(input, in);
		char* save_name = strtok(input, "SAVE");

		char* cwd = _getcwd(NULL, 0);
		if (cwd == NULL) {
			fprintf(stdout, "Unable to save\n");
			return 0;
		}
		strcat(cwd, "\\");
		strcat(cwd, save_name);
		fprintf(stdout, "save name: %s\n", cwd);
		return save_game(save_name);
	}

	else {
		// regular move
		(*out)[0] = malloc(sizeof(char*));
		(*out)[1] = malloc(sizeof(char*));
		(*out)[2] = malloc(sizeof(char*));

		if (sscanf(in, "%s %s %s", (*out)[0], (*out)[1], (*out)[2]) != 3) {
			fprintf(stderr, "Failed to read input\n");
			return 0;
		}

		if (strcmp(game_info.board.layout[atoi((*out)[1]) - 1][atoi((*out)[2]) - 1], ".."))
			return 0;

		if (game_info.turn == 1)
			return 1;
		else {
			return check_move_validity(atoi((*out)[2]) - 1, atoi((*out)[1]) - 1);
		}
	}
}

void remove_card_from_hand(player_t* player, int card_index) {
	for (int i = card_index; i < player->cards_num - 1; i++) {
		player->cards[i] = player->cards[i + 1];
	}

	player->cards[player->cards_num] = "\0";
	player->cards_num--;
}

void handle_player_input(player_t* player) {
	fprintf(stdout, "Hand(%i): ", (*player).id);

	for (int i = 0; i < (*player).cards_num; i++) {
		fprintf(stdout, "%s ", (*player).cards[i]);
	}
	fprintf(stdout, "\n");

	int valid = 0;
	char buffer[255];

	char** move_cmd = malloc(sizeof(char*));
	do {
		fprintf(stdout, "Move? ");
		fgets(buffer, 255, stdin);
	} while (!parse_move(buffer, &move_cmd));

	if (move_cmd == NULL)
		return;

	// Arrays start at 0!
	game_info.board.layout[atoi(move_cmd[2]) - 1][atoi(move_cmd[1]) - 1] = (*player).cards[atoi(move_cmd[0]) - 1];

	remove_card_from_hand(player, atoi(move_cmd[0]) - 1);
}

player_t create_player(int num, enum player_type type) {
	player_t ret;
	ret.cards = malloc(sizeof(char*));
	ret.cards_num = 0;
	ret.id = num;
	ret.type = type;

	return ret;
}

int deal_card(player_t* player) {
	if (game_info.card_index > game_info.card_count)
		return 0;

	player->cards[player->cards_num] = malloc(sizeof(char*));
	strcpy(player->cards[player->cards_num], game_info.deck[game_info.card_index]);

	player->cards_num++;
	game_info.card_index++;
	return 1;
}

void init_player_hands() {
	for (int i = 0; i < game_info.card_count; i++) {
		while (game_info.players[0].cards_num != 5) {
			if (!deal_card(&game_info.players[0]))
				return;
		}

		while (game_info.players[1].cards_num != 5) {
			if (!deal_card(&game_info.players[1]))
				return;
		}
	}
}

void init_game() {
	generate_board();
	init_player_hands();
	display_board();
}

int play_turn() {
	if (!deal_card(&game_info.players[game_info.current_player]))
		return 0;

	if (game_info.players[game_info.current_player].type == PLAYER_HUMAN) {
		handle_player_input(&game_info.players[game_info.current_player]);
	}

	display_board();

	return 1;
}

int new_game(char* deck, int w, int h, char* p1type, char* p2type) {
	game_info.type = TYPE_NEW;

	game_info.deckname = deck;

	// p1type not h or a, or p2type not h or a, width not between 3 and 100, height not between 3 and 100
	if ((strcmp(p1type, "h") && strcmp(p1type, "a")) || (strcmp(p2type, "h") && strcmp(p2type, "a")) || ((w < 3) || (w > 100)) || ((h < 3) || (h > 100))) {
		fprintf(stderr, "Incorrect arg types\n");
		return EXIT_INVALID_PLAYER;
	}

	// Set game_info member variables
	game_info.board.row_count = h;
	game_info.board.col_count = w;

	enum player_type temp_type;
	if (!strcmp(p1type, "h"))
		temp_type = PLAYER_HUMAN;
	else
		temp_type = PLAYER_AI;

	game_info.players[0] = create_player(1, temp_type);

	if (!strcmp(p2type, "h"))
		temp_type = PLAYER_HUMAN;
	else
		temp_type = PLAYER_AI;

	game_info.players[1] = create_player(2, temp_type);

	if (parse_deckfile(deck, &game_info.card_count)) {
		fprintf(stderr, "Unable to parse deckfile\n");
		return EXIT_INVALID_DECK;
	}

	if (game_info.card_count < 11)
		return EXIT_INVALID_DECK;

	init_game();

	while (1) {
		if (is_board_full()) {
			fprintf(stderr, "Board full\n");
			return EXIT_BOARD_FULL;
		}

		if (!play_turn())
			break;

		// Flip whose turn it is
		game_info.current_player = !game_info.current_player;

		if (!game_info.current_player)
			game_info.turn++;
	}

	return EXIT_NORMAL;
}

int resume_game(char* savefile, char* p1type, char* p2type) {
	game_info.type = TYPE_RESUME;

	// p1type not h or a, or p2type not h or a, width not between 3 and 100, height not between 3 and 100
	if ((p1type != 'h' && p1type != 'a') || (p2type != 'h' && p2type != 'a')) {
		fprintf(stderr, "Incorrect arg types\n");
		return EXIT_INVALID_PLAYER;
	}

	if (parse_savefile(savefile)) {
		fprintf(stderr, "Unable to parse savefile\n");
		return EXIT_INVALID_SAVE;
	}
	return EXIT_NORMAL;
}

// Ensures all members in game_info have a default value
void init_game_info(int w, int h) {
	game_info.current_player = 0;
	game_info.card_count = 0;
	game_info.players = malloc(sizeof(player_t*));
	game_info.board.layout = malloc(sizeof(char*));
	game_info.deck = malloc(sizeof(char*));
	game_info.turn = 1;
}

// new game: bark deckfile width height p1type p2type
// saved game: bark savefile p1type p2type
int main(int argc, char** argv) {
	// Incorrect number of args
	if (argc != 6 && argc != 4) {
		fprintf(stderr, "Usage: bark savefile p1type p2type\r\nbark deck width height p1type p2type\n");
		return EXIT_INCORRECT_ARGS;
	}

	init_game_info(argv[2], argv[3]);

	// New game
	if (argc == 6) {
		return new_game(argv[1], atoi(argv[2]), atoi(argv[3]), argv[4], argv[5]);
	}

	// Load game
	else if (argc == 4) {
		return resume_game(argv[1], argv[2], argv[3]);
	}
}